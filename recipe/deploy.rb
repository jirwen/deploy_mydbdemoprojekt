set :application_name,         "updatedb"
set :module_name,              "demo"

set :cleanup_workspace_before, false

set :log_level,                "INFO"
set :log_file,                 "deploy.log"
set :log_file_level,           "DEBUG"

Deployment.deliver do
  $log.writer.info "Starting ..."

  # get latest configuration for this app from cdb
  $log.writer.info "Get latest parameters from cdb"
  config = get_all_config_parameters
  # testing some ssh stuff
  Net::SSH.start(config['remote_host'], config['remote_user'], \
                 :auth_methods  => ['publickey'], \
                 :forward_agent => true, \
                 :keys          => config['ssh_key_file'], \
                 :timeout       => 30 ) do |session|
    session.open_channel do |ch|
      ch.exec "hostname -a" do |ch, success|
        raise "could not execute command" unless success

        # "on_data" is called when the process writes something to stdout
        ch.on_data do |c, type, data|
          raise "could not execute command" if type.nil?
          $log.writer.info type
          $log.writer.info data
        end

        # "on_extended_data" is called when the process writes something to stderr
        ch.on_extended_data do |c, type, data|
          $log.writer.error "could not execute command" unless type.nil?
          #$log.writer.info type
          $log.writer.error data
          exit 1 
        end
      end
    end
  end
  #end testing zone
  $log.writer.info "create mysqldump from #{config['mysqldb_source_db']}"
  dump_out = remote_execute("mysqldump --defaults-file=#{config['mysqldb_source']} -u#{config['mysqldb_user_source']} -p#{config['mysqldb_pw_source']} #{config['mysqldb_source_db']} > #{config['mysqldb_backup_dir']}/#{config['mysqldb_source_db']}.dmp")
  $log.writer.info dump_out

  $log.writer.info "import mysqldump to #{config['mysqldb_target_db']}"
  dump_in = remote_execute("mysql --defaults-file=#{config['mysqldb_target']} -u#{config['mysqldb_user_target']} -p#{config['mysqldb_pw_target']} #{config['mysqldb_target_db']} < #{config['mysqldb_backup_dir']}/#{config['mysqldb_source_db']}.dmp")
  $log.writer.info dump_in

  $log.writer.info "Upload additional sql to database server"
  $log.writer.info "Send mail to deployment team #{config['deploy_email_to']}"
  report_by_mail

  $log.writer.info "Deployment done!"
end
